---
# Amhir-Language        ---> always update system before installing software. Update weekly. Don't wait to long. <---
---


### How to change language and spellcheck. Systemwide.

![image10](/uploads/2f863289b92b73a2f4db214965cd74b8/image10.png)

## [VIDEO TUTORIAL --> url:link](https://gitlab.com/hubmus/amhir-language/-/blob/main/Arabic-keyboard-layout.mp4?expanded=true&viewer=rich)

#### NAVIGATION RECONFIGURE LOCALES FROM THE TERMINAL

run the following inside the terminal :

`$ dpkg-reconfigure locales`


- arrow up/down
- SPACE bar = select/unselect
- TAB = next option [OK/CANCEL]


`Reboot after a change`

_examples_---------------------------

nl_NL.UTF-8

de_DE.UTF-8

en_GB.UTF-8


##### List of landcodes
https://docs.moodle.org/dev/Table_of_locales#Table

![reconfigure-locales](/uploads/931fca21dad8733d132b7bf108aba900/reconfigure-locales.png)


---
---
# How to change keyboard layout - 



![proxy-image](/uploads/59f3823e199c9bd98ab537d49b480c2f/proxy-image.jpeg)

#### Install with Synaptic package manager

$ xfce4-xkb-plugin 
